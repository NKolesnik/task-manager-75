package ru.t1consulting.nkolesnik.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private ProjectDto project;

    public AbstractProjectResponse(@Nullable final ProjectDto project) {
        this.project = project;
    }

}

