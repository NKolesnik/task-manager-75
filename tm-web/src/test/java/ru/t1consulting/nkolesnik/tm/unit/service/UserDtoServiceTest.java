package ru.t1consulting.nkolesnik.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1consulting.nkolesnik.tm.api.service.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.config.ApplicationConfiguration;
import ru.t1consulting.nkolesnik.tm.enumerated.RoleType;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyLoginException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyPasswordException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyRoleException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.marker.UnitCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.UserDto;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class UserDtoServiceTest {

    @NotNull
    private static final String USER_LOGIN_TEST = "USER_FOR_TEST";

    @NotNull
    private static final String USER_PASSWORD_TEST = "USER_FOR_TEST";

    @NotNull
    private static final RoleType USER_ROLE_TYPE_TEST = RoleType.USUAL;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void createUser() {
        Assert.assertThrows(UserEmptyLoginException.class,
                () -> userService.createUser("", USER_PASSWORD_TEST, USER_ROLE_TYPE_TEST));
        Assert.assertThrows(UserEmptyPasswordException.class,
                () -> userService.createUser(USER_LOGIN_TEST, null, USER_ROLE_TYPE_TEST));
        Assert.assertThrows(UserEmptyRoleException.class,
                () -> userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin(USER_LOGIN_TEST));
        userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, USER_ROLE_TYPE_TEST);
        @NotNull final UserDto userDto = userService.findByLogin(USER_LOGIN_TEST);
        Assert.assertEquals(USER_LOGIN_TEST, userDto.getLogin());
        userService.delete(userDto);
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(UserEmptyLoginException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin(USER_LOGIN_TEST));
        userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, USER_ROLE_TYPE_TEST);
        @NotNull final UserDto userDto = userService.findByLogin(USER_LOGIN_TEST);
        Assert.assertEquals(USER_LOGIN_TEST, userDto.getLogin());
        userService.delete(userDto);
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin("ABC_USER"));
        userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, USER_ROLE_TYPE_TEST);
        @NotNull final UserDto userDto = userService.findByLogin(USER_LOGIN_TEST);
        Assert.assertEquals(USER_LOGIN_TEST, userDto.getLogin());
        userService.delete(userDto);
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin(USER_LOGIN_TEST));
    }

}