package ru.t1consulting.nkolesnik.tm.integration.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1consulting.nkolesnik.tm.marker.WebCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.Result;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(WebCategory.class)
public class TaskRestEndpointClientTest {

    @NotNull
    private static final String apiUrl = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();
    private static final int COUNT_TASKS = 4;
    @NotNull
    private static String sessionId;
    @NotNull
    private static List<TaskDto> tasks;

    @NotNull
    private TaskDto task;

    @BeforeClass
    @SneakyThrows
    public static void setUpClass() {
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final String authUrl = "http://localhost:8080/api/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = template.getForEntity(authUrl, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        final List<HttpCookie> cookies =
                HttpCookie.parse(responseHeaders.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(httpCookie -> "JSESSIONID".equals(httpCookie.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<TaskDto> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity entity
    ) {
        @NotNull final RestTemplate template = new RestTemplate();
        return template.exchange(url, method, entity, TaskDto.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Before
    public void setUp() {
        tasks = new ArrayList<>();
        @NotNull final String url = apiUrl + "save";
        for (int i = 0; i < COUNT_TASKS; i++) {
            @NotNull final TaskDto taskDto = new TaskDto();
            taskDto.setName("PROJ_" + i);
            tasks.add(taskDto);
        }
        for (TaskDto taskDto : tasks) {
            sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDto>(taskDto, header));
        }
        task = tasks.get(0);

    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String clearUrl = apiUrl + "clear";
        sendRequest(clearUrl, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @Test
    public void create() {
        @NotNull final String url = apiUrl + "create";
        @NotNull final ResponseEntity<TaskDto> response = sendRequest(
                url,
                HttpMethod.POST,
                new HttpEntity<>(header)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final TaskDto taskDto = response.getBody();
        Assert.assertNotNull(taskDto);
    }

    @Test
    public void existsById() {
        @NotNull final String taskId = task.getId();
        @NotNull final String url = apiUrl + "existsById/" + taskId;
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Result> response = template.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(header),
                Result.class);
        Assert.assertNotNull(response.getBody());
        boolean exists = response.getBody().getSuccess();
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        @NotNull final String taskId = task.getId();
        @NotNull final String url = apiUrl + "findById/" + taskId;
        @NotNull final ResponseEntity<TaskDto> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String url = apiUrl + "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<TaskDto[]> response =
                (template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), TaskDto[].class));
        Assert.assertNotNull(response.getBody());
        TaskDto[] tasks = response.getBody();
        Assert.assertTrue(Arrays.stream(tasks).anyMatch(p -> task.getId().equals(p.getId())));
    }

    @Test
    public void count() {
        @NotNull final String url = apiUrl + "count";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Long> response =
                template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(COUNT_TASKS, response.getBody().longValue());
    }

    @Test
    public void save() {
        @NotNull final String url = apiUrl + "save";
        @NotNull final String taskId = task.getId();
        @NotNull final String expected = "NEW NAME";
        task.setName(expected);
        sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDto>(task, header));
        @NotNull final String findUrl = apiUrl + "findById/" + taskId;
        @NotNull final ResponseEntity<TaskDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto taskDto = response.getBody();
        Assert.assertEquals(expected, taskDto.getName());
    }

    @Test
    public void delete() {
        @NotNull final String url = apiUrl + "delete";
        @NotNull final String taskId = task.getId();
        @NotNull final String findUrl = apiUrl + "findById/" + taskId;
        @NotNull ResponseEntity<TaskDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDto>(task, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteById() {
        @NotNull final String taskId = task.getId();
        @NotNull final String url = apiUrl + "deleteById/" + taskId;
        @NotNull final String findUrl = apiUrl + "findById/" + taskId;
        @NotNull ResponseEntity<TaskDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteAll() {
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task);
        @NotNull final String taskId = task.getId();
        @NotNull final String url = apiUrl + "deleteAll";
        @NotNull final String findUrl = apiUrl + "findById/" + taskId;
        @NotNull ResponseEntity<TaskDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(tasks, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void clear() {
        @NotNull final String taskId = task.getId();
        @NotNull final String url = apiUrl + "clear";
        @NotNull final String findUrl = apiUrl + "findById/" + taskId;
        @NotNull final String findAllUrl = apiUrl + "findAll";
        @NotNull ResponseEntity<TaskDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDto taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<TaskDto[]> responseEntity =
                template.exchange(findAllUrl, HttpMethod.GET, new HttpEntity<>(header), TaskDto[].class);
        Assert.assertNotNull(responseEntity.getBody());
        int countTasks = responseEntity.getBody().length;
        Assert.assertEquals(0, countTasks);
    }

}