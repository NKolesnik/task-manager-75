package ru.t1consulting.nkolesnik.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}