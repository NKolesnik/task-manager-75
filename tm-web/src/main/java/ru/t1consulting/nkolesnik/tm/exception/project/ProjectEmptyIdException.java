package ru.t1consulting.nkolesnik.tm.exception.project;

import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class ProjectEmptyIdException extends AbstractException {

    public ProjectEmptyIdException() {
        super("Error! Project id is empty...");
    }

}
