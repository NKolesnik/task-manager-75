package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

    boolean existsByUserIdAndId(String userId, String id);

    TaskDto findByUserIdAndId(String userId, String id);

    List<TaskDto> findAllByUserId(String userId);

    long countByUserId(String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    @Modifying
    @Transactional
    void deleteByUserId(String userId);

}
