package ru.t1consulting.nkolesnik.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

    void add(@Nullable ProjectDto project);

    void add(@Nullable String userId, @Nullable ProjectDto project);

    void add(@Nullable Collection<ProjectDto> projects);

    void add(@Nullable String userId, @Nullable Collection<ProjectDto> projects);

    void set(@Nullable Collection<ProjectDto> projects);

    void set(@Nullable String userId, @Nullable Collection<ProjectDto> projects);

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name, @Nullable String description);

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    List<ProjectDto> findAll();

    @NotNull
    List<ProjectDto> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDto> findAll(@Nullable Comparator<ProjectDto> comparator);

    @NotNull
    List<ProjectDto> findAll(@Nullable Sort sort);

    @NotNull
    List<ProjectDto> findAll(@Nullable String userId, @Nullable Comparator<ProjectDto> comparator);

    @NotNull
    List<ProjectDto> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    ProjectDto findById(@Nullable String id);

    @Nullable
    ProjectDto findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void update(@Nullable ProjectDto project);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable ProjectDto project);

    void remove(@Nullable String userId, @Nullable ProjectDto project);

    void removeById(@Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

}
