package ru.t1consulting.nkolesnik.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @NotNull
    @Value("---")
    public String emptyValue;

    @NotNull
    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @NotNull
    @Value("#{environment['server.host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['session.key']}")
    public String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    public Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.url']}")
    public String databaseConnectionString;

    @NotNull
    @Value("#{environment['database.username']}")
    public String databaseUsername;

    @Nullable
    @Value("#{environment['database.password']}")
    public String databasePassword;

    @NotNull
    @Value("#{environment['database.driver']}")
    public String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    public String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    public String databaseHbm2Ddl;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    public String databaseShowSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    public String databaseFormatSql;

    @NotNull
    @Value("#{environment['cache.use_second_lvl_cache']}")
    public String cacheUseSecondLvlCache;

    @NotNull
    @Value("#{environment['cache.provider_config_file']}")
    public String cacheProviderConfigFile;

    @NotNull
    @Value("#{environment['cache.use_query_cache']}")
    public String cacheUseQueryCache;

    @NotNull
    @Value("#{environment['cache.use_min_puts']}")
    public String cacheUseMinPuts;

    @NotNull
    @Value("#{environment['cache.region_prefix']}")
    public String cacheRegionPrefix;

    @NotNull
    @Value("#{environment['cache.region.factory_class']}")
    public String cacheRegionFactoryClass;

    @NotNull
    @Value("#{environment['cache.hazelcast.use_lite_member']}")
    public String cacheHazelcastUseLiteMember;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

}