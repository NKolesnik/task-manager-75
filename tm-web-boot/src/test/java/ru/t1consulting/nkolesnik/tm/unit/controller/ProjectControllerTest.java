package ru.t1consulting.nkolesnik.tm.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.marker.UnitCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectControllerTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/project/";

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private ProjectDto project;

    @NotNull
    private String userId;

    @Before
    public void setUp() {
        project = new ProjectDto();
        project.setName("PROJ");
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        Object principal;
        Object credentials;
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        projectService.save(userId, project);
    }

    @After
    public void tearDown() {
        projectService.clear(userId);
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = API_URL + "create";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        @Nullable final List<ProjectDto> projects = projectService.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void list() {
        @NotNull final String url = "http://localhost:8080/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void edit() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = API_URL + "edit/" + projectId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void editPost() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = API_URL + "edit/" + projectId;
        @NotNull final String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = API_URL + "delete/" + projectId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        Assert.assertNull(projectService.findById(userId, projectId));
    }
}