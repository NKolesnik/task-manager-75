package ru.t1consulting.nkolesnik.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1consulting.nkolesnik.tm.marker.UnitCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectEndpointImplTest {

    @NotNull
    private final static String PROJECT_API_URL = "http://localhost:8080/api/projects/";

    @Nullable
    private String userId = null;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext ctx;

    @NotNull
    private ProjectDto project = new ProjectDto();

    private void auth() {
        mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
    }

    @Before
    public void setUp() {
        auth();
        saveProject(project);
    }

    @After
    public void tearDown() {
        cleanUp();
    }

    @SneakyThrows
    private void createProject(@NotNull final ProjectDto projectDto) {
        @NotNull final String url = PROJECT_API_URL + "create";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void create() throws Exception {
        cleanUp();
        ProjectDto projectDto = new ProjectDto();
        Assert.assertNotNull(userId);
        long count = countProjects();
        Assert.assertEquals(0L, count);
        createProject(projectDto);
        count = countProjects();
        Assert.assertEquals(1L, count);
    }

    @SneakyThrows
    private boolean existsProjectById(@NotNull final String id) {
        @NotNull final String existsByIdUrl = PROJECT_API_URL + "existsById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(existsByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Boolean.class);
    }

    @Test
    public void existsById() {
        @NotNull final String projectId = project.getId();
        boolean exists = existsProjectById(projectId);
        Assert.assertTrue(exists);
        Assert.assertFalse(existsProjectById(UUID.randomUUID().toString()));
    }

    @SneakyThrows
    private ProjectDto findProjectById(@NotNull final String id) {
        @NotNull final String findByIdUrl = PROJECT_API_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(findByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        if (json.isEmpty()) return null;
        return mapper.readValue(json, ProjectDto.class);
    }

    @Test
    public void findById() {
        @NotNull final String projectId = project.getId();
        @Nullable final ProjectDto projectDto = findProjectById(projectId);
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectId, projectDto.getId());
    }

    @SneakyThrows
    private List<ProjectDto> findAllProjects() {
        @NotNull final String existsByIdUrl = PROJECT_API_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(existsByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return Arrays.asList(mapper.readValue(json, ProjectDto[].class));
    }

    @Test
    public void findAll() {
        long count = countProjects();
        Assert.assertEquals(1L, count);
        @NotNull final ProjectDto projectById = findProjectById(project.getId());
        @NotNull final List<ProjectDto> projects = findAllProjects();
        @NotNull final ProjectDto projectDto = projects.get(0);
        Assert.assertEquals(projectById.getId(), projectDto.getId());
    }

    @SneakyThrows
    public long countProjects() {
        @NotNull final String countUrl = PROJECT_API_URL + "count";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(countUrl).contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class);
    }

    @Test
    public void count() {
        long count = countProjects();
        Assert.assertEquals(1L, count);
    }

    @SneakyThrows
    private void saveProject(@NotNull final ProjectDto projectDto) {
        @NotNull final String url = PROJECT_API_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void save() {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setName("TEST");
        Assert.assertNotNull(userId);
        projectDto.setUserId(userId);
        @NotNull final String projectId = projectDto.getId();
        saveProject(projectDto);
        ProjectDto projectById = findProjectById(projectId);
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectId, projectById.getId());
        Assert.assertEquals("TEST", projectById.getName());
        Assert.assertEquals(userId, projectById.getUserId());
    }

    @SneakyThrows
    private void deleteProject(@NotNull final ProjectDto projectDto) {
        @NotNull final String url = PROJECT_API_URL + "delete";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void delete() {
        long count = countProjects();
        Assert.assertEquals(1L, count);
        deleteProject(project);
        count = countProjects();
        Assert.assertEquals(0L, count);
    }

    @SneakyThrows
    private void deleteProjectById(@NotNull final String id) {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_API_URL + "deleteById/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteById() {
        @NotNull final String projectId = project.getId();
        @Nullable ProjectDto projectDto = findProjectById(projectId);
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectId, projectDto.getId());
        deleteProjectById(projectId);
        projectDto = findProjectById(projectId);
        Assert.assertNull(projectDto);
    }

    @SneakyThrows
    private void deleteProjectList(@NotNull final List<ProjectDto> projects) {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(
                MockMvcRequestBuilders.post(PROJECT_API_URL + "deleteAll")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteAll() {
        @NotNull final String projectId = project.getId();
        @Nullable ProjectDto projectDto = findProjectById(projectId);
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectId, projectDto.getId());
        List<ProjectDto> projects = new ArrayList<>();
        projects.add(projectDto);
        deleteProjectList(projects);
        projectDto = findProjectById(projectId);
        Assert.assertNull(projectDto);
    }

    @SneakyThrows
    private void cleanUp() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_API_URL + "clear")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void clear() {
        long count = countProjects();
        Assert.assertEquals(1L, count);
        cleanUp();
        count = countProjects();
        Assert.assertEquals(0L, count);
    }

}