package ru.t1consulting.nkolesnik.tm.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.marker.UnitCategory;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;
import ru.t1consulting.nkolesnik.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskControllerTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/task/";

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private TaskDto task;

    @NotNull
    private String userId;

    @Before
    public void setUp() {
        task = new TaskDto();
        task.setName("TASK");
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        Object principal;
        Object credentials;
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        taskService.save(userId, task);
    }

    @After
    public void tearDown() {
        taskService.clear(userId);
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = API_URL + "create";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        @Nullable final List<TaskDto> tasks = taskService.findAll(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void list() {
        @NotNull final String url = "http://localhost:8080/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void edit() {
        @NotNull final String taskId = task.getId();
        @NotNull final String url = API_URL + "edit/" + taskId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void editPost() {
        @NotNull final String taskId = task.getId();
        @NotNull final String url = API_URL + "edit/" + taskId;
        @NotNull final String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String taskId = task.getId();
        @NotNull final String url = API_URL + "delete/" + taskId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        Assert.assertNull(taskService.findById(userId, taskId));
    }

}