package ru.t1consulting.nkolesnik.tm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1consulting.nkolesnik.tm.enumerated.RoleType;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "role")
public class RoleDto extends AbstractModelDto {

    private static final long serialVersionUID = 1;

    @ManyToOne
    private UserDto user;

    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USUAL;

    @Override
    public String toString() {
        return roleType.name();
    }

}
