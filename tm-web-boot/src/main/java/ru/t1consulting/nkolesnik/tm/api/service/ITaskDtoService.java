package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.List;

public interface ITaskDtoService {

    TaskDto create(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @NotNull String id);

    @Nullable TaskDto findById(@Nullable final String userId, @NotNull String id);

    @Nullable List<TaskDto> findAll(@Nullable final String userId);

    long count(@Nullable final String userId);

    void save(@Nullable final String userId, @NotNull TaskDto task);

    void deleteById(@Nullable final String userId, @NotNull String id);

    void delete(@Nullable final String userId, @NotNull TaskDto task);

    void deleteAll(@Nullable final String userId, @NotNull List<TaskDto> taskList);

    void clear(@Nullable final String userId);

}
