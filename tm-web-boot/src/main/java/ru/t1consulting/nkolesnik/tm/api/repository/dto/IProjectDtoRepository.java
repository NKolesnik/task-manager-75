package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.List;

public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    boolean existsByUserIdAndId(String userId, String id);

    ProjectDto findByUserIdAndId(String userId, String id);

    List<ProjectDto> findAllByUserId(String userId);

    long countByUserId(String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    @Modifying
    @Transactional
    void deleteByUserId(String userId);

}
