package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.service.ProjectDtoService;
import ru.t1consulting.nkolesnik.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @Override
    @WebMethod
    @PostMapping("/create")
    public ProjectDto create() {
        return projectService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        projectService.save(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        projectService.delete(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        projectService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody final List<ProjectDto> projects
    ) {
        projectService.deleteAll(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear(UserUtil.getUserId());
    }

}
