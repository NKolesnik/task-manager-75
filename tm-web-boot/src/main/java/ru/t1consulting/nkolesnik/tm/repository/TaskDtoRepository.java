package ru.t1consulting.nkolesnik.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

public interface TaskDtoRepository extends JpaRepository<TaskDto, String> {

}
